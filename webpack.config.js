var path = require("path");

module.exports = {
  entry: "./src/index.js",
  output: {
    filename: "index.js",
    path: path.resolve(__dirname, "dist")
  },
  watch: true,
  devServer: {
    inline: true,

    port: 3000,

    publicPath: "/",

    setup(app) {
      var bodyParser = require("body-parser");
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      //app.use(bodyParser.json());

      let form = `
              <form>
                  <div class="form-group">
                      <label for="recipient-name" class="control-label">Recipient:</label>
                      <input type="text" class="form-control" id="recipient-name" name="recipient-name">
                  </div>
                  <div class="form-group">
                      <label for="message-text" class="control-label">Message:</label>
                      <textarea class="form-control" id="message-text" name="message-text"></textarea>
                  </div>
              </form>
            `;

      app.get("/form-data", function (req, res) {
        res.send(form);
      });

      app.post("/form-data", function (req, res) {
        if (req.body["recipient-name"] === "") {

          const validationResult = `
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Errors!</strong> Recipient cannot be blank.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            `;
          res.status(422).send(validationResult + form);
        } else {
          res.status(204).send();
        }
      });

      app.delete('/form-data/:id', function (req, res) {
        var id = req.param("id");
        res.status(204).send();
      });
    }
  }
};