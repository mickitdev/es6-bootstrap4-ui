import {
    Button
} from "./ui/button";
import {
    ModalAlert
} from "./ui/modal-alert";
import {
    ModalConfirmation
} from "./ui/modal-confirmation";
import {
    ModalEditor
} from "./ui/modal-editor";
import * as service from "./services/form-service-html";
import {
    Alert
} from "./ui/alert";

const container = $("#main");

const alertButton = new Button("Bootstrap alert!");
alertButton.createElement();
alertButton.appendToElement(container);

alertButton.element.on("click", () => {
    ModalAlert.init({
        body: "Something went wrong, try again..."
    }).open();
});

const confirmationButton = new Button("Bootstrap confirmation!");
confirmationButton.createElement();
confirmationButton.appendToElement($("#main"));

confirmationButton.element.on("click", () => {
    const alert = ModalConfirmation.init({
        body: "Are you really sure!",
        onConfirmed: () => {
            service.remove("/form-data/999")
                .done(() => window.alert('Removed'))
                .fail(() => window.alert('Failed to remove'));
        }
    });
    alert.open();
});

const editorButton = new Button("Bootstrap editor!");
editorButton.createElement();
editorButton.appendToElement($("#main"));

editorButton.element.on("click", () => {
    const editor = ModalEditor.init({
        title: "Edit",
        dataSource: {
            get: () => service.get("/form-data"),
            post: (data) => service.createOrUpdate("/form-data", data)
        },
        onSaved: () => window.alert("OnSaved executed")
    });
    editor.open();
});

const alertBtn = new Button("Alert!");
alertBtn.createElement();
alertBtn.appendToElement($("#main"));

let count = 0

alertBtn.element.on("click", () => {
    const alert = new Alert({
        body: "Test " + count,
        class: "alert-danger",
        replace: true,
        dismissible: true
    });
    alert.createElement();
    alert.appendToElement($("#alerts"))
    count++;
});

const alertBtnLifetime = new Button("Alert lifetime!");
alertBtnLifetime.createElement();
alertBtnLifetime.appendToElement($("#main"));

alertBtnLifetime.element.on("click", () => {
    const alert = new Alert({
        body: "Test " + count,
        class: "alert-success",
        lifeTime: 3000
    });
    alert.createElement();
    alert.appendToElement($("#alerts"))
    count++;
});