import {
  BaseElement
} from "./base-element";

let defaults = {
  title: "Alert",
  body: ""
}

export class ModalAlert extends BaseElement {

  static init(options = {}) {
    let alert = new ModalAlert(options);
    alert.createElement();
    alert.appendToElement($("body"));
    return alert;
  }

  constructor(options = {}) {
    super(options);
    this.settings = Object.assign({}, defaults, options);
  }

  createElement() {
    super.createElement();
    this.element.on('hidden.bs.modal', () => this.element.remove());
  }

  getElementString() {
    return `
        <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">${this.settings.title}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              ${this.settings.body}
            </div>
            <div class="modal-footer">
              ${this.buttons()}
            </div>
          </div>
        </div>
      </div>
        `;
  }

  buttons() {
    return `
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    `;
  }

  open() {
    this.element.modal("show");
  }

  close() {
    this.element.modal("hide");
  }
}