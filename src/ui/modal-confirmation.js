import {
    ModalAlert
} from "./modal-alert";

let defaults = {
    title: "Please confirm",
    body: "Are you sure you want to do this?",
    onConfirmed: () => { 
        throw "OnConfirm not implemented through options";
    }
}

export class ModalConfirmation extends ModalAlert {

    static init(options = {}) {
        let confirmBox = new ModalConfirmation(options);
        confirmBox.createElement();
        confirmBox.appendToElement($("body"));
        return confirmBox;
    }

    constructor(options = {}) {
        super(options);
        this.settings = Object.assign({}, defaults, options);
    }

    createElement() {
        super.createElement();
        this.element.on("click", ".js-btn-confirm", () => {
            this.element.on('hidden.bs.modal', () => this.settings.onConfirmed());
            this.close();
        });
      }

    buttons() {
        return `
          <button type="button" class="btn btn-danger js-btn-confirm">Confirm</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        `;
    }
}