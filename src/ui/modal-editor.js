import {
    ModalAlert
} from "./modal-alert";

let defaults = {
    title: "Edit",
    body: "",
    dataSource: {
        get: () => {
            throw "get not implemented";
        },
        post: () => {
            throw "post not implemented";
        }
    },
    onSaved: () => {}
}

export class ModalEditor extends ModalAlert {

    static init(options = {}) {
        let editor = new ModalEditor(options);
        editor.createElement();
        editor.appendToElement($("body"));
        return editor;
    }

    constructor(options = {}) {
        super(options);
        this.settings = Object.assign({}, defaults, options);
    }

    createElement() {
        super.createElement();
        this.element.on("click", ".js-btn-confirm", () => {
            this.saveForm().done(() => {
                this.close();
                this.settings.onSaved();
            }).fail((response) => {
                this.element.find(".modal-body").html(response["responseText"]);
            });
        });
        this.element.on('show.bs.modal', () => {
            this.settings.dataSource.get().done((data) => {
                this.element.find(".modal-body").html(data);
            });
        });
    }

    saveForm() {
        const form = this.element.find("form");
        const data = form.serialize();
        return this.settings.dataSource.post(data);
    }

    buttons() {
        return `
          <button type="button" class="btn btn-success js-btn-confirm">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        `;
    }
}