import {
    BaseElement
} from "./base-element";

let defaults = {
    body: "",
    class: "alert-primary",
    replace: false,
    dismissible: false,
    lifeTime: undefined
}

export class Alert extends BaseElement {
    constructor(options = {}) {
        super(options);
        this.settings = Object.assign({}, defaults, options);
    }

    appendToElement(el) {
        this.createElement();
        if (this.settings.replace) {
            el.empty().append(this.element);
        } else {
            el.append(this.element);
        }
        if (this.settings.lifeTime !== undefined) {
            setTimeout(this.fadeOutElement, this.settings.lifeTime, this.element);
        }
    }

    fadeOutElement(el) {
        el.fadeOut(300, "linear", () => el.alert('close'));
    }

    getElementString() {
        return `
        <div class="alert ${this.settings.class} ${this.settings.dismissible ? 'alert-dismissible fade show' : ''}" role="alert">
            <div class="alert-content">${this.settings.body}</div>
            ${this.buttonDissmis()}
        </div>
        `;
    }

    buttonDissmis() {
        const button = this.settings.dismissible ? `
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>` : "";
        return button;
    }
}