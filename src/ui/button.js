import {
    BaseElement
} from './base-element';

export class Button extends BaseElement {

    constructor(title, bootstrapClass = null) {
        super(title, bootstrapClass);
        this.title = title;
        if (bootstrapClass === null){
            this.bootstrapClass = "btn-primary"
        }
    }

    getElementString() {
        return `
            <button type="button" class="btn ${this.bootstrapClass}">${this.title}</button>
        `;
    }
}