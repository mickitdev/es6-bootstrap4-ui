const get = (url) => {
    let options = {
        url: url,
        type: "GET",
        dataType: "html"
    };
    return $.ajax(options);
};

const createOrUpdate = (url, data) => {
    let options = {
        url: url,
        type: "POST",
        data: data,
        dataType: "html"
    };
    return $.ajax(options);
};

const remove = (url) => {
    let options = {
        url: url,
        type: "DELETE"
    };
    return $.ajax(options);
}


export { get, createOrUpdate, remove };